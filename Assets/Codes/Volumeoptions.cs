﻿using UnityEngine;
using System.Collections;

public class Volumeoptions : MonoBehaviour {

    //Äänenvoimakkuuden säätö muuttuja
    private float mastervolume = 0.5f;
    
    //Äänenvoimakkuuden näyttämiseen tarkoitetut muuttujat interfacea varten
    private float displayvolume = 50;
    private GameObject textvolume = null;

	// Use this for initialization
	void Start () 
    {
        this.textvolume = GameObject.Find("Textvolume");
	}
	
	// Update is called once per frame
	void Update () 
    {
        //L näppäin lisää volumea 0.1f eli 10%
        if (Input.GetKeyDown(KeyCode.L))
        {
            if (this.displayvolume < 100)
            {
                this.mastervolume = this.mastervolume + 0.1f;
                this.displayvolume = this.displayvolume + 10;
                AudioListener.volume = this.mastervolume;

                this.textvolume.GetComponent<TextMesh>().text = "Äänenvoimakkuus: " + this.displayvolume + "%";
            }

            if (this.displayvolume == 100)
            {
                this.textvolume.GetComponent<TextMesh>().text = "Äänet ovat täysillä";
            }
        }

        //K näppäin pienentää volumea 0.1f eli 10%
        if (Input.GetKeyDown(KeyCode.K))
        {
            if (this.displayvolume > 0)
            {
                this.mastervolume = this.mastervolume - 0.1f;
                this.displayvolume = this.displayvolume - 10;
                AudioListener.volume = this.mastervolume;

                this.textvolume.GetComponent<TextMesh>().text = "Äänenvoimakkuus: " + this.displayvolume + "%";
            }

            if (this.displayvolume == 0)
            {
                this.textvolume.GetComponent<TextMesh>().text = "Äänet ovat mykistetty";
            }
        }
	}
}
