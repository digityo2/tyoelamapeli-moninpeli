﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

//Alkuvalikon koodit joilla vaihdetaan sceneä nappeja painamalla
public class NappiKoodi : MonoBehaviour {

    public void loadLevelGame () 
    {
        SceneManager.LoadScene(1);

    }
    public void EndGame()
    {
        Application.Quit();      
    }
}
