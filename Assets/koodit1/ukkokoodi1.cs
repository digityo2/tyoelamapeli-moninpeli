﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityStandardAssets.Characters.ThirdPerson;

public class ukkokoodi1 : NetworkBehaviour {

    [SyncVar]
    public int malli = 3;

    [SyncVar]
    public string nimi = "PELAAJA";

    public Avatar avatar1 = null;
    public Avatar avatar2 = null;
    public Avatar avatar3 = null;
    public Avatar avatar4 = null;
    public Avatar avatar5 = null;
    public Avatar avatar6 = null;
    public Avatar avatar7 = null;

    private int malli2 = 3;

    void OnGUI()
    {
        if (isLocalPlayer)
        {
            this.nimi = GUI.TextField(new Rect(25f, Screen.height - 40f, 200f, 30f),this.nimi);
            if (GUI.Button(new Rect(240f, Screen.height - 40f, 100f, 30f),"Lähetä"))
            {
                this.CmdMuutaNimi(this.nimi);
            }  // if
        }
    }  // OnGUI

    [Command]
    public void CmdMuutaMalli(int uusimalli)
    {
        this.malli = uusimalli;
    }  // CmdMuutaMalli

    [Command]
    public void CmdMuutaNimi(string uusinimi)
    {
        this.nimi = uusinimi;
    }  // CmdMuutaNimi


    void Start () {
	    if (isLocalPlayer)
        {
            this.GetComponent<ukkoohjaus1>().enabled = true;
        }  // if
	}  // Start


	
	void Update () {

        // Vain localplayer voi muuttaa mallia
        if (isLocalPlayer)
        {
            if (Input.GetKeyUp(KeyCode.I))
            {
               // this.CmdMuutaMalli(1);
            }

            if (Input.GetKeyUp(KeyCode.O))
            {
               // this.CmdMuutaMalli(2);
            }

            if (Input.GetKeyUp(KeyCode.P))
            {
                //this.CmdMuutaMalli(3);
                if (this.malli2 < 7)
                {
                    this.malli2++;
                }
                else
                {
                    this.malli2 = 1;
                }

                this.CmdMuutaMalli(this.malli2);
            }
        }  // if

        if (this.malli == 1)
        {
            this.GetComponent<Animator>().avatar = this.avatar1;
            this.GetComponent<Transform>().GetChild(0).gameObject.SetActive(true);
            this.GetComponent<Transform>().GetChild(1).gameObject.SetActive(false);
            this.GetComponent<Transform>().GetChild(2).gameObject.SetActive(false);
            this.GetComponent<Transform>().GetChild(3).gameObject.SetActive(false);
            this.GetComponent<Transform>().GetChild(4).gameObject.SetActive(false);
            this.GetComponent<Transform>().GetChild(5).gameObject.SetActive(false);
            this.GetComponent<Transform>().GetChild(6).gameObject.SetActive(false);
        }  // if

        if (this.malli == 2)
        {
            this.GetComponent<Animator>().avatar = this.avatar2;
            this.GetComponent<Transform>().GetChild(0).gameObject.SetActive(false);
            this.GetComponent<Transform>().GetChild(1).gameObject.SetActive(true);
            this.GetComponent<Transform>().GetChild(2).gameObject.SetActive(false);
            this.GetComponent<Transform>().GetChild(3).gameObject.SetActive(false);
            this.GetComponent<Transform>().GetChild(4).gameObject.SetActive(false);
            this.GetComponent<Transform>().GetChild(5).gameObject.SetActive(false);
            this.GetComponent<Transform>().GetChild(6).gameObject.SetActive(false);
        }  // if

        if (this.malli == 3)
        {
            this.GetComponent<Animator>().avatar = this.avatar3;
            this.GetComponent<Transform>().GetChild(0).gameObject.SetActive(false);
            this.GetComponent<Transform>().GetChild(1).gameObject.SetActive(false);
            this.GetComponent<Transform>().GetChild(2).gameObject.SetActive(true);
            this.GetComponent<Transform>().GetChild(3).gameObject.SetActive(false);
            this.GetComponent<Transform>().GetChild(4).gameObject.SetActive(false);
            this.GetComponent<Transform>().GetChild(5).gameObject.SetActive(false);
            this.GetComponent<Transform>().GetChild(6).gameObject.SetActive(false);
        }  // if

        if (this.malli == 4)
        {
            this.GetComponent<Animator>().avatar = this.avatar4;
            this.GetComponent<Transform>().GetChild(0).gameObject.SetActive(false);
            this.GetComponent<Transform>().GetChild(1).gameObject.SetActive(false);
            this.GetComponent<Transform>().GetChild(2).gameObject.SetActive(false);
            this.GetComponent<Transform>().GetChild(3).gameObject.SetActive(true);
            this.GetComponent<Transform>().GetChild(4).gameObject.SetActive(false);
            this.GetComponent<Transform>().GetChild(5).gameObject.SetActive(false);
            this.GetComponent<Transform>().GetChild(6).gameObject.SetActive(false);
        }  // if

        if (this.malli == 5)
        {
            this.GetComponent<Animator>().avatar = this.avatar5;
            this.GetComponent<Transform>().GetChild(0).gameObject.SetActive(false);
            this.GetComponent<Transform>().GetChild(1).gameObject.SetActive(false);
            this.GetComponent<Transform>().GetChild(2).gameObject.SetActive(false);
            this.GetComponent<Transform>().GetChild(3).gameObject.SetActive(false);
            this.GetComponent<Transform>().GetChild(4).gameObject.SetActive(true);
            this.GetComponent<Transform>().GetChild(5).gameObject.SetActive(false);
            this.GetComponent<Transform>().GetChild(6).gameObject.SetActive(false);
        }  // if

        if (this.malli == 6)
        {
            this.GetComponent<Animator>().avatar = this.avatar6;
            this.GetComponent<Transform>().GetChild(0).gameObject.SetActive(false);
            this.GetComponent<Transform>().GetChild(1).gameObject.SetActive(false);
            this.GetComponent<Transform>().GetChild(2).gameObject.SetActive(false);
            this.GetComponent<Transform>().GetChild(3).gameObject.SetActive(false);
            this.GetComponent<Transform>().GetChild(4).gameObject.SetActive(false);
            this.GetComponent<Transform>().GetChild(5).gameObject.SetActive(true);
            this.GetComponent<Transform>().GetChild(6).gameObject.SetActive(false);
        }  // if

        if (this.malli == 7)
        {
            this.GetComponent<Animator>().avatar = this.avatar7;
            this.GetComponent<Transform>().GetChild(0).gameObject.SetActive(false);
            this.GetComponent<Transform>().GetChild(1).gameObject.SetActive(false);
            this.GetComponent<Transform>().GetChild(2).gameObject.SetActive(false);
            this.GetComponent<Transform>().GetChild(3).gameObject.SetActive(false);
            this.GetComponent<Transform>().GetChild(4).gameObject.SetActive(false);
            this.GetComponent<Transform>().GetChild(5).gameObject.SetActive(false);
            this.GetComponent<Transform>().GetChild(6).gameObject.SetActive(true);
        }  // if

        this.GetComponentInChildren<TextMesh>().text = this.nimi;

    }  // Update
}  // class
