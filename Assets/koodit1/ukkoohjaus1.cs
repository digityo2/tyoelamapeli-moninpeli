﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ukkoohjaus1 : MonoBehaviour {
    private Animator ani1 = null;

    private int kameramoodi = 2;

    void Start () {
        this.ani1 = this.GetComponent<Animator>();
        this.AsetaKameraMoodi(this.kameramoodi);
    }  // Start

    private void AsetaKameraMoodi(int moodi)
    {
        if (moodi == 1)
        {
            Camera.main.transform.position = this.transform.position -
                                             this.transform.forward * 4 +
                                             this.transform.up * 3;
            //Camera.main.transform.LookAt(this.transform.position);
            //Quaternion apu = Camera.main.transform.rotation;
            //apu.x = 0f;
            //Camera.main.transform.rotation = apu;
            Camera.main.transform.parent = this.transform;
            this.kameramoodi = 1;
        }
        else if (moodi == 2)
        {
            Camera.main.transform.parent = null;
            SmoothCameraFollow.target = this.transform;
            this.kameramoodi = 2;
        }
    }  // VaihdaKameraMoodi

    void Update () {

        // Kierrot
		if (Input.GetButton("Horizontal") && (Input.GetAxisRaw("Horizontal") < 0 ))
        {
            this.ani1.SetFloat("kierto", 0.5f);
        }  // if

        if (Input.GetButton("Horizontal") && (Input.GetAxisRaw("Horizontal") > 0))
        {
            this.ani1.SetFloat("kierto", 1f);
        }  // if

        if (Input.GetButtonUp("Horizontal"))
        {
            this.ani1.SetFloat("kierto", 0f);
        }  // if

        // Liike
        if (Input.GetButton("Vertical") && (Input.GetAxisRaw("Vertical") > 0))
        {
            if (Input.GetKey(KeyCode.LeftShift))
            {
                this.ani1.SetFloat("liike", 1f);
            }
            else
            {
                this.ani1.SetFloat("liike", 0f);
            }  // if

            this.ani1.Play("kavely1");
        }  // if

        if (Input.GetButtonUp("Vertical"))
        {
            this.ani1.Play("kierrot_blend");
        }  // if

        if (Input.GetButtonUp("Moodi"))
        {
                if (this.kameramoodi == 1)
                {
                    this.AsetaKameraMoodi(2);
                }
                else
                {
                    this.AsetaKameraMoodi(1);
                }
        } // if
        Debug.Log("Paikka : " + this.GetComponent<Transform>().position);
    }  // Update
}  // class
