﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

//DoorToOtherScenes1 script löytyy DoorToOtherScenes1 -> scenechange objektista

public class DoorToOtherScenes1 : MonoBehaviour {

	void Start () 
    {
	
	}

	void Update () 
    {
	
	}

    //DoorToOtherScenes1 scenechange objektin collider tunnistus
    void OnTriggerEnter(Collider other)
    {
        //Kun oveen kävelee vaihtuu scene alla olevaan
        SceneManager.LoadScene(0);
    }
}
